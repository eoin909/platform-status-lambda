@Library(value='pipeline-library@master', changelog=false)
import com.genesys.jenkins.Service

final serviceBuild = new com.genesys.jenkins.Service()

pipeline {
  agent {
    node {
      label 'dev_mesos||dev_shared'
      customWorkspace "${JOB_NAME}-${currentBuild.number}"
    }
  }

  options {
    ansiColor('xterm')
    buildDiscarder(logRotator(numToKeepStr: '45'))
    timeout(time: 1, unit: 'HOURS')
    timestamps()
    skipStagesAfterUnstable()
    disableConcurrentBuilds()
  }

  parameters {
    string(name: 'BRANCH_TO_BUILD', defaultValue: 'master', description: 'Which branch to build from.')
    string(name: 'DEPLOY_PATH', defaultValue: 'dev', description: 'The sequence of environments to deploy into (comma-separated).')
    string(name: 'PIPELINES_BRANCH_TO_BUILD', defaultValue: 'master', description: 'Which branch of ai-group-pipelines repo to build with.')
  }

  environment {
    SERVICE_NAME = 'journey-platform-status'
    SERVICE_ROLE = "${env.SERVICE_NAME}"
    BRANCH_DEPLOY_PATH = "${params.DEPLOY_PATH}"
    DEPLOY_SCHEDULE = 'always'
    DEPLOY_TYPE = 'skynet'
    EMAIL_LIST = 'Altocloud-Dev@genesys.com'
    PIPELINES_REPO_PATH = "${env.WORKSPACE}/pipelines"
    TEST_JOB = 'no-tests'
  }

  stages {
    stage('Set up workspace') {
      steps {
        script {
          currentBuild.description = "Deploy ${BRANCH_TO_BUILD} to ${BRANCH_DEPLOY_PATH}"

          // clone pipelines repo
          dir('pipelines') {
            git branch: params.PIPELINES_BRANCH_TO_BUILD,
                url: 'git@bitbucket.org:inindca/ai-group-pipelines.git',
                changelog: false
          }
        }
      }
    }

    stage('Bundle Lambda function') {
      steps {
        script {
          serviceBuild.initialize(null)
          serviceBuild.bundleLambda("--lambda-path ${WORKSPACE} --type go", "${WORKSPACE}/deploy/infra", "platformStatus", null, null)
        }
      }
    }

    stage('Bricks post build') {
      steps {
        script {
          serviceBuild.preBuild(null)
          serviceBuild.postBuild(true)
          serviceBuild.bricksPostBuild(env.SERVICE_NAME)
        }
      }
    }
  }
}
