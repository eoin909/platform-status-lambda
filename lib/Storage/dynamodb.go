package Storage

import (
	"log"
	Models "../model"
	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/dynamodb"
	"github.com/aws/aws-sdk-go/service/dynamodb/dynamodbattribute"
)

func InsertOne(app Models.ApplicationsDynamoDB) {
	config := &aws.Config{
		Region:   aws.String("us-west-2"),
		Endpoint: aws.String("http://local-dev:8000"),
	}
	sess := session.Must(session.NewSession(config))
	svc := dynamodb.New(sess)
	av, err := dynamodbattribute.MarshalMap(app)

	input := &dynamodb.PutItemInput{
		Item:      av,
		TableName: aws.String("journey-applications-status"),
	}

	_, err = svc.PutItem(input)

	if err != nil {
		log.Fatalln(err)
	}

	log.Println("We have inserted a new item!")
}
