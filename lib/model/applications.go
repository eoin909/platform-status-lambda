package model

type ApplicationsAPIResponse struct {
	Applications 		[]Application		`json:"applications"`
}

type Application struct {
	Id                 int                `json:"id"`
	Name               string             `json:"name"`
	Language           string             `json:"language"`
	HealthStatus       string             `json:"health_status"`
	Reporting          bool               `json:"reporting"`
	ApplicationSummary ApplicationSummary `json:"application_summary"`
}

type ApplicationSummary struct {
	ResponseTime 	float64 	`json:"response_time"`
	Throughput 		float64 	`json:"throughput"`
	ErrorRate 		float64 	`json:"error_rate"`
	ApdexTarget 	float64 	`json:"apdex_target"`
	ApdexScore 		float64 	`json:"apdex_score"`
	HostCount 		int 		`json:"host_count"`
	InstanceCount 	int 		`json:"instance_count"`
}

type ApplicationDynamoDB struct {
	Name 				string       	`json:"name"`
	RequestsPerMinute 	float64   		`json:"requestsPerMinute"`
	ErrorRate 			float64 		`json:"errorRate"`
	ApdexScore 			float64 		`json:"apdexScore"`
	HostCount 			int 			`json:"hostCount"`
}

type ApplicationsDynamoDB struct {
	Period 			string 			`json:"period"`
	Applications 		[]ApplicationDynamoDB	`json:"applications"`
}
