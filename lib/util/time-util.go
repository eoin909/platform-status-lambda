package util

import (
	"math"
	"strings"
	"time"
)

func GetStartAndEndDate(minutes int, roundToNearestMinutes int64) (string, string) {
	currentDate := time.Now()
	secs := math.Round(float64(currentDate.Unix()/(roundToNearestMinutes * 60)) * 60 * float64(roundToNearestMinutes))
	dateRoundedToNearest5Minutes := time.Unix(int64(secs), 0)

	startTime := dateRoundedToNearest5Minutes.Add(time.Duration(minutes * 2) * time.Minute)
	endTime := dateRoundedToNearest5Minutes.Add(time.Duration(minutes) * time.Minute)

	startDate := convertTimeToISOString(startTime)
	endDate := convertTimeToISOString(endTime)

	return startDate, endDate
}

func convertTimeToISOString(date time.Time) string {
	return strings.Split(date.Format(time.RFC3339), "+")[0] + ".000Z"
}

