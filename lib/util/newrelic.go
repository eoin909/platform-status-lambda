package util

import (
	Models "../model"
	"encoding/json"
	"io/ioutil"
	"log"
	"net/http"
)

func GetApplication(applicationName string, startDate string, endDate string) Models.Application {
	client := &http.Client{}
	baseUrl := "https://api.newrelic.com/v2/applications.json"
	req, err := http.NewRequest("GET", baseUrl, nil)
	q := req.URL.Query()

	req.Header.Add( "X-Api-Key", "6050cf7788812c7ee3cc5858fd5713af")
	req.Header.Add(      "Content-Type", "application/json")

	q.Add("filter[name]", applicationName)
	q.Add("from", startDate)
	q.Add("to", endDate)

	req.URL.RawQuery = q.Encode()

	resp, err := client.Do(req)

	if err != nil {
		log.Fatalln(err)
	}

	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)

	if err != nil {
		log.Fatalln(err)
	}

	s, err := parseNewRelicResponse([]byte(body))

	if err != nil {
		log.Fatalln(err)
	}

	log.Println(applicationName)

	return s.Applications[0]
}

func parseNewRelicResponse(body []byte) (*Models.ApplicationsAPIResponse, error) {
	var s = new(Models.ApplicationsAPIResponse)
	err := json.Unmarshal(body, &s)

	if err != nil {
		log.Fatalln(err)
	}

	return s, err
}