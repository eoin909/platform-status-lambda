package main

//import (
//	Models "./lib/model"
//	Storage "./lib/storage"
//	Util "./lib/util"
//)
//
//func main() {
//	appNames := [18]string{
//		"event-collector-service (Test)",
//		"event-store-service-java (Test)",
//		"outcome-event-scoring (Test)",
//		"journey-context-service (Test)",
//		"journey-outcome-score-service (Test)",
//		"journey-reporting-service (Test)",
//		"journey-rules-engine (Test)",
//		"journey-session-store (Test)",
//		"action-map-service (Test)",
//		"action-map-service-java (Test)",
//		"context-service (Test)",
//		"customer-service (Test)",
//		"outcome-score-service (Test)",
//		"rule-service (Test)",
//		"snippet-service (Test)",
//		"websocket-service (Test)",
//	}
//	startDate, endDate := Util.GetStartAndEndDate(-5, 5)
//
//	apps:= Models.ApplicationsDynamoDB {
//		Period: endDate,
//		Applications: []Models.ApplicationDynamoDB{},
//	}
//
//	for _, appName := range appNames {
//		application := Util.GetApplication(appName, startDate, endDate)
//
//		app := Models.ApplicationDynamoDB {
//			Name: application.Name,
//			RequestsPerMinute: application.ApplicationSummary.Throughput,
//			ErrorRate: application.ApplicationSummary.ErrorRate,
//			ApdexScore: application.ApplicationSummary.ApdexScore,
//			HostCount: application.ApplicationSummary.HostCount,
//		}
//
//		apps.Applications = append(apps.Applications, app)
//	}
//
//	Storage.InsertOne(apps)
//}

import (
	"fmt"
	"github.com/aws/aws-lambda-go/lambda"
	"log"
)

type Request struct {
	ID        float64 `json:"id"`
	Value     string  `json:"value"`
}

type Response struct {
	Message string `json:"message"`
	Ok      bool   `json:"ok"`
}

func Handler(request Request) (Response, error) {
	log.Println("handler called")

	return Response{
		Message: fmt.Sprintf("Processed request ID %f", request.ID),
		Ok:      true,
	}, nil
}

func main() {
	log.Println("lambda called")
	lambda.Start(Handler)
}